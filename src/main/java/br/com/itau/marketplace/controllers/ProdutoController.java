package br.com.itau.marketplace.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.services.ProdutoService;

@RestController
@RequestMapping("/produto")
public class ProdutoController {
	@Autowired
	ProdutoService produtoService;

	@GetMapping
	public Iterable<Produto> listarProdutos() {
		return produtoService.buscarProdutos();
	}
	
	@GetMapping("/{id}")
	public Produto listarProdutoPorId(@PathVariable int id) {
		return produtoService.buscarProdutoPorId(id);
	}
	
	@PostMapping
	public void inserirProduto(@RequestBody Produto produto) {
		produtoService.inserir(produto);
	}
}
