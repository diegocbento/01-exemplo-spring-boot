package br.com.itau.marketplace.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.marketplace.models.Produto;
import br.com.itau.marketplace.repositories.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	ProdutoRepository produtoRepository;
	
	public Iterable<Produto> buscarProdutos(){
		return produtoRepository.findAll();
	}
	
	public Produto buscarProdutoPorId(int id) {
		Optional<Produto> produtoOptional = produtoRepository.findById(id);
		
		if(produtoOptional.isPresent()) {
			return produtoOptional.get();
		}
		
		return null;
	}
	
	public void inserir(Produto produto) {
		produtoRepository.save(produto);
	}
}
